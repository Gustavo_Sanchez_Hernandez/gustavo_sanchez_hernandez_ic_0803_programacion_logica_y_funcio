

public class Banco {
    private String nombrePais;
    private Cliente []clientes;
    private Persona []personas;

    Banco(String nombrePais, int tam_personas, int tam_clientes){
        this.nombrePais = nombrePais;
        this.personas = new Persona[tam_personas];
        this.clientes = new Cliente[tam_clientes];
        this.construirPersonas();
        this.construirClientes();
    }

    private void construirPersonas(){
        for(int i = 0; i < this.getTotalPersonas(); i++){
            this.personas[i] = new Persona();
        }
    }

    private void construirClientes(){
        for(int i = 0; i < this.getTotalClientes(); i++){
            this.clientes[i] = new Cliente();
        }
    }

    public int getTotalPersonas(){
        return this.personas.length;
    }

    public int getTotalClientes(){
        return this.clientes.length;
    }

    public boolean isTodoOcupado(){
        boolean todo_ocupado = false;
        int contador = 0;
        for(int i = 0; i < this.getTotalPersonas(); i++){
            if(this.getIsOcupadoPersona(i)){
                contador++;
            }
        }
        if(contador == this.getTotalPersonas())
            todo_ocupado = true;
        return todo_ocupado;
    }

    public boolean isTodoVacio(){
        boolean todo_vacio = false;
        int contador = 0;
        for(int i = 0; i < this.getTotalPersonas(); i++){
            if(!this.getIsOcupadoPersona(i)){
                contador++;
            }
        }
        if(contador == this.getTotalPersonas())
            todo_vacio = true;
        return todo_vacio;
    }

    public boolean existeEspacioVacio(){
        boolean existeVacio = false;
        for(int i = 0; i < this.getTotalPersonas(); i++){
            if(!this.getIsOcupadoPersona(i)){
                existeVacio = true;
                break;
            }
        }
        return existeVacio;
    }

    public boolean existeEspacioOcupado(){
        boolean existeOcupado = false;
        for(int i = 0; i < this.getTotalPersonas(); i++){
            if(this.getIsOcupadoPersona(i)){
                existeOcupado = true;
                break;
            }
        }
        return existeOcupado;
    }

    public int indiceVacio(){
        int index_vacio = -1;
        for(int i = 0; i < this.getTotalPersonas(); i++){
            if(!this.getIsOcupadoPersona(i)){
                index_vacio = i;
                break;
            }
        }
        return index_vacio;
    }

    public boolean printSoloOcupados(int pais){
        boolean imprimio = false;
        if(this.existeEspacioOcupado()){
            imprimio = true;
            System.out.println("----------------------------------------------------------------------------------------------------------------");
            System.out.println("\tDel pais [" + pais + "] " + this.nombrePais + " los clientes disponibles son: ");
            for(int  i = 0; i < this.getTotalPersonas(); i++){
                if(this.getIsOcupadoPersona(i)){
                    System.out.println("El cliente " + i + " es: ");
                    this.printPersona(i);
                }
            }
            System.out.println("----------------------------------------------------------------------------------------------------------------");
        }
        return imprimio;
    }

    public void printTodosClientes(int pais){
        System.out.println("\tDel pais [" + pais + "] " + this.nombrePais + ": ");
        for(int  i = 0; i < this.getTotalPersonas(); i++){
            System.out.println("El cliente " + i + " es: ");
            this.printPersona(i);
        }
    }

    public void imprimirEspecifico(int persona){
        System.out.println("----------------------------------------------------------------------------------------------------------------");
        System.out.println("\tDel pais " + this.nombrePais + ": ");
        this.printPersona(persona);
        System.out.println("----------------------------------------------------------------------------------------------------------------");
    }

    public String getNombrePais() {
        return nombrePais;
    }

    public void setNombrePais(String nombrePais) {
        this.nombrePais = nombrePais;
    }

    public Persona getEspecificoCliente(int index){
        return this.personas[index];
    }

    public void setEspecificoCliente(int index, Persona persona){
        //this.clientes[index] = cliente;
        this.personas[index].setTodosDatos(persona);
    }

    public String getNombrePersona(int index){
        return this.personas[index].getNombre();
    }

    public char getSexoPersona(int index){
        return this.personas[index].getSexo();
    }

    public double getSaldoCliente(int index){
        return this.clientes[index].getSaldo();
    }

    public  boolean getIsOcupadoPersona(int index){
        return this.personas[index].isOcupado();
    }

    public void eliminarCliente(int index){
        this.clientes[index].eliminar();
        this.personas[index].eliminar();
    }

    public void cambiarNombrePersona(int index, String nuevoNombre){
        this.personas[index].cambiarNombre(nuevoNombre);
    }

    public void cambiarSexoPersona(int index, char nuevoSexo){
        this.personas[index].cambiarSexo(nuevoSexo);
    }

    public void abonarSaldoCliente(int index, double cuanto){
        this.clientes[index].abonarSaldo(cuanto);
    }

    public void retirarSaldoCliente(int index, double cuanto){
        this.clientes[index].retirarSaldo(cuanto);
    }

    public void printCliente(int index){
        this.clientes[index].imprimirCliente();
    }

    public void printPersona(int index){
        this.personas[index].imprimirPersona();
    }
}