public class Cliente {
    private double saldo;

    Cliente(){
        this.setTodosDatos();
    }

    Cliente(double saldo){
        this.setTodosDatos(saldo);
    }

    Cliente(Cliente cliente){
        this.setTodosDatos(cliente);
    }

    private void setTodosDatos(){
        this.saldo = 0.0;
    }

    public void setTodosDatos(double saldo){
        this.validarSaldo(saldo);
    }

    public void setTodosDatos(Cliente cliente){
        this.validarSaldo(cliente.getSaldo());
    }

    public Cliente getTodosDatos(){
        Cliente temporal = new Cliente();
        temporal.saldo = this.getSaldo();
        return temporal;
    }


    private void validarSaldo(double saldo){
        if(saldo <= 0.0){
            saldo = 0.0;
        }
        this.saldo = saldo;
    }

    private boolean tieneSaldo(){
        return this.saldo > 0.0;
    }

    public double getSaldo() {
        return saldo;
    }

    public void eliminar(){
        this.setTodosDatos();
    }

    public void abonarSaldo(double cuanto){
        
            if(cuanto > 0.0){
                this.saldo += cuanto;
            }
            else{
                System.out.println("\tImposible, no puedes abonar saldos negativos o nada.");
            }
        
    }

    public void retirarSaldo(double cuanto){
        
            if(cuanto > 0.0){
                if(this.tieneSaldo()){
                    if(this.saldo >= cuanto)
                        this.saldo -= cuanto;
                    else
                        System.out.println("\tImposible, tú saldo es insuficiente para retirar " + cuanto + ".");
                }
                else
                    System.out.println("\tImposible, no hay saldo.");
            }
            else
                System.out.println("\tImposible, no puedes retirar saldos negativos o nada.");
        
    }

    @Override
    public String toString(){
        return "| -> Saldo: |" + this.saldo + "|";
    }

    public void imprimirCliente(){
        System.out.println("\t\t" + this.toString());
    }
}