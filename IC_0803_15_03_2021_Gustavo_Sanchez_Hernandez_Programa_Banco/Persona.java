public class Persona {
    private String nombre;
    private char sexo;
    private boolean ocupado;

    Persona(){
        this.setTodosDatos();
    }

    Persona(String nombre, char sexo){
        this.setTodosDatos(nombre, sexo);
    }

    Persona(Persona persona){
        this.setTodosDatos(persona);
    }

    private void setTodosDatos(){
        this.nombre = null;
        this.sexo = ' ';
        this.ocupado = false;
    }

    public void setTodosDatos(String nombre, char sexo){
        this.nombre = nombre;
        this.validarSexo(sexo);
        this.setOcupado();
    }

    public void setTodosDatos(Persona persona){
        this.nombre = persona.getNombre();
        this.validarSexo(persona.getSexo());
        this.setOcupado();
    }

    public Persona getTodosDatos(){
        Persona temporal = new Persona();
        temporal.nombre = this.getNombre();
        temporal.sexo = this.getSexo();
        temporal.ocupado = this.isOcupado();
        return temporal;
    }

    private void validarSexo(char sexo){
        if(sexo != 'M' && sexo != 'F'){
            sexo = ' ';
        }
        this.sexo = sexo;
    }

    private boolean tieneDatos(){
        return !(this.nombre == null || this.nombre.equalsIgnoreCase(" ") || this.sexo == ' ');
        /*if(this.nombre == null || this.nombre.equalsIgnoreCase(" ") || this.sexo == ' ')
            return false;
        else
            return true;*/
    }

    private void setOcupado() {
        if(!this.tieneDatos()){
            this.setTodosDatos();
        }
        else{
            this.ocupado = true;
        }
    }

    public String getNombre(){
        return this.nombre;
    }

    public char getSexo() {
        return sexo;
    }

    public boolean isOcupado() {
        return ocupado;
    }

    public void eliminar(){
        this.setTodosDatos();
    }

    public void cambiarNombre(String nuevoNombre){
        if(nuevoNombre != null && !nuevoNombre.equalsIgnoreCase(" ")){
            this.nombre = nuevoNombre;
        }
        this.setOcupado();
    }

    public void cambiarSexo(char sexo){
        if(sexo == 'M' || sexo == 'F'){
            this.sexo = sexo;
        }
        this.setOcupado();
    }

    @Override
    public String toString(){
        return "-> Nombre: |" + this.nombre +
                "| -> Sexo: |" + this.sexo +
                "| -> Ocupado: |" + this.ocupado + "|";
    }

    public void imprimirPersona(){
        System.out.println("\t\t" + this.toString());
    }
}