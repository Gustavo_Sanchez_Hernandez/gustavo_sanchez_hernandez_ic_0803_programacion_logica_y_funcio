import java.util.List;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
public class FP_Functional_01{
	public static void main(String[] args){
		List<Integer> numbers = List.of(12,9,13,4,6,2,4,12,15);
		 List<String> courses  = List.of("palab Spring","Spring Boot","API","Microservices","AWS","PCF","Azure","Docker","Kumbernetes");
		
		//Ejercicio 1
		 System.out.println("\n ");
		numbers.stream()
				.filter(number -> number % 2 != 0)
				.forEach(number -> System.out.print(number+" | "));
				System.out.println("\n ");
		//Ejerciico 2
				courses.stream().forEach(palabras-> System.out.print(palabras+" | "));
				System.out.println("\n ");
		//Ejercicio 3
				courses.stream().filter(palabra -> palabra.contains("Spring"))
				.forEach(FP_Functional_01::print);
				System.out.println("\n ");

		FP_Functional_01.letrasNumbers(courses);
       
        FP_Functional_01.Cube_ImparesNumbers(numbers);
        
        FP_Functional_01.printNumbers_CharactersNumbers(courses);
        

       
	}

private static void print(String palabra){
             System.out.print(palabra + " | ");
        }
	//Ejercicio 4
     public static void letrasNumbers(List<String>courses){
       courses.forEach(new Consumer<String>(){
           public void accept(final String name){
               if(name.length()>3){
                   System.out.println(name);
               }
           }
       });
       System.out.println("\n ");
     }
     //Ejercicio 5
     public static void Cube_ImparesNumbers(List<Integer>numbers){
         for (int i = 0; i < numbers.size(); i++){
             int a = (int)numbers.get(i);
             if (a % 2 != 0){
                 int res= (int)Math.pow(a, 3);
                 System.out.println(a+" al cubo "+"= "+res);
             }
         }
         System.out.println("\n ");
     }
     //Ejercicio 6 
     public static void printNumbers_CharactersNumbers(List<String>courses){
        courses.forEach(new Consumer<String>(){
            public void accept(final String name){
                System.out.print(name+"= "+name.length()+"\n");
            }
        });

     }
}