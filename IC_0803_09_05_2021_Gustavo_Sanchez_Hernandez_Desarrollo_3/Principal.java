public class Principal{
	public static void main(String[] args) {
	Principal.engine((int x, int y) ->x*y,(long x, long y) ->x*y);	
	//System.out.println("==> CalculadoraInt --> resultado = " + Principal.engine_1().calculate(5,6));
		//System.out.println("==> CalculadoraLong --> resultado = " + Principal.engine_2().calculate(6,2));
	}

	//retorna un objeto de tipo "Calculadora int"
	private static void engine(CalculadoraInt num,CalculadoraLong cal){
		int x=5,y=6;
		int resultado=num.calculate(x,y);
		System.out.println("Resultado int: "+resultado);

		long a=6,b=2;
		long resultado2=cal.calculate(a,b);
		System.out.println("Resultado long: "+resultado2);
	}

	/*private static void engine(CalculadoraLong cal){
		//return (x, y) -> x * y;
		long x=6,y=2;
		long resultado=cal.calculate(x,y);
		System.out.println("Resultado long: "+resultado);

	}*/
}