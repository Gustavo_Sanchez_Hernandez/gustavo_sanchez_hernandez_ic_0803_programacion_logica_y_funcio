public interface PruebaExamen{

    default public void mensajeHola(){
        System.out.println("Hola mi nombre es: Gustavo S�nchez Hern�ndez");
    }

    default public void mensajeHola(String cadena){
		System.out.println("La cadena es: "+ cadena);
	}

    public static int operacionPrueba(int a, int b){
        
		return a+b;
	}

	public void operacionPrueba(int x, int y, int z);
}