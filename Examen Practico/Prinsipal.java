//PASO 1
public class Prinsipal{
    //PASO 2
    
    public static void main(String[] args){
        //PASO 3
        PruebaExamen pe = ( x, y, z) -> {System.out.println(x+y+z);};
        //PASO 4
        pe.operacionPrueba(1, 2, 3);
        //PASO 5
        int r=PruebaExamen.operacionPrueba(5, 7);
        System.out.println(r);
        //PASO 6
        pe.mensajeHola();
        //PASO 7
        pe.mensajeHola("Examen");
        //PASO 8
        Prinsipal.engine((x, y, z) -> System.out.println(x+(y*z)), 1,PruebaExamen.operacionPrueba(3, 4),PruebaExamen.operacionPrueba(5, 9) );
        //PASO 12 Y 13
        Prinsipal.miMetodo((int x,int y,int z) -> System.out.println(x+(y/z)), 1,PruebaExamen.operacionPrueba(5, 7),PruebaExamen.operacionPrueba(3, 4) );
        
        //Prinsipal.miMetodo((double x,double y,double z) -> System.out.println(x+(y/z)), 1,PruebaExamen.operacionPrueba(5, 7),PruebaExamen.operacionPrueba(3, 4) );
    }
    //PASO 9
    public static void engine(PruebaExamen calc, int x, int y, int z){
        calc.operacionPrueba(x, y, z);
    }
    //PASO 10
    private static void miMetodo(PruebaExamen cal,int x, int y, int z){
        //PASO 11
        cal.operacionPrueba(x, y, z);
        
    }
    
}