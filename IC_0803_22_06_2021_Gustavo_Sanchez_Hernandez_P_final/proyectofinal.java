
import javax.swing.plaf.nimbus.State;
public class proyectofinal {
//int argc, char const *argv[]
    
    
    public static int main(int argc,char conste, String[] argv) {
        int LIMITE_INPUTS_SIMULACION = 10;
        int estado_actual;
        int contador = 0;
        int[] array_inputs = null;

        int goNORTE = 0;
        int waitNORTE = 1;
        int goESTE = 2;
        int waitESTE = 3;

        int NO_CARROS = 0;
        int CARROS_ESTE = 1;
        int CARROS_NORTE = 2;
        int AMBOS_LADOS = 3;
        // Simulando algunas entradas
        array_inputs[0] = NO_CARROS;
        array_inputs[1] = CARROS_ESTE;
        array_inputs[2] = CARROS_ESTE;
        array_inputs[3] = AMBOS_LADOS;
        array_inputs[4] = CARROS_NORTE;
        array_inputs[5] = CARROS_NORTE;
        array_inputs[6] = CARROS_NORTE;
        array_inputs[7] = AMBOS_LADOS;
        array_inputs[8] = CARROS_ESTE;
        array_inputs[9] = NO_CARROS;

        estado_actual = goNORTE;
        
        while(contador < LIMITE_INPUTS_SIMULACION){
        //printf("\t------> contador = [%d]\n", contador); 
        estado_actual = get_CurrentState(FSM, estado_actual, array_inputs[contador]);
        
        //FSM engine
        execute_state(FSM, estado_actual);
        contador++;  
    };
    

    return 0;
    }
}
